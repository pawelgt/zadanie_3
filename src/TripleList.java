import java.util.Iterator;

/**
 * Created by Pawel on 2015-12-23.
 */
public class TripleList<T extends Comparable> implements Iterable<Node<T>> {
    private Node<T> head;
    private int size;

    public TripleList() {
        this.head = new Node<>(null);
        size = 0;
    }

    public TripleList(T[] values) {
        this.head = new Node<>(null);
        size = 0;

        for (int i = 0; i < values.length; i++) {
            add(new Node<>(values[i]));
        }
    }

    public void clear() {
        this.head.setNext(null);
        this.size = 0;
    }

    private int compare(Node<T> node1, Node<T> node2) {
        //System.out.println(node1.getValue() + "  " + node2.getValue() + "  = " + node1.compare(node2.getValue()));
        return node1.compare(node2.getValue());
    }

    public void add(Node<T> node) {
        if(head.getNext() == null) {
            head.setNext(node);
            node.setPrevious(head);
            node.setIndex(++size);
        }
        else {
            Node<T> last = head.getNext();
            while (last.getNext() != null) {
                last = last.getNext();
            }
            if (last.getMiddle() == null) {
                last.setMiddle(node);
                node.setMiddle(last);
                node.setIndex(++size);
            } else {
                last.setNext(node);
                node.setPrevious(last);
                node.setIndex(++size);
            }

//            System.out.println("node:" + node.getValue() + " last:" + last.getValue());
        }
    }

    public String printList() {
        Node<T> node = head.getNext();
        String res = "";

        while(node != null) {
            res += node.getValue() + " ";
            if(node.getMiddle() != null && node.getMiddle().checkVertical()) {
                res += "\\" +node.getMiddle().getValue() + "/ ";
            }
            node = node.getNext();
        }

        return res;
    }

    public String printIndex() {
        Node<T> node = head.getNext();
        String res = "";

        while(node != null) {
            res += node.getIndex() + " ";
            if(node.getMiddle() != null && node.getMiddle().checkVertical()) {
                res += "\\" +node.getMiddle().getIndex() + "/ ";
            }
            node = node.getNext();
        }

        return res;
    }

    private void checkList() {
        Node<T> node = head.getNext();
        while(node != null) {
            if(node.getMiddle() == null && node.getNext() != null) {
                System.out.println("ERROR!");
                Node<T> next = node.getNext();
                node.setNext(null);
                next.clearConnections();
                add(next);
                break;
            }
            node = node.getNext();
        }
    }

    public int getSize() {
        return size;
    }

    public Node<T> getHead() {
        return head;
    }

    public Node<T> getFirst() {
        return head.getNext();
    }

    public Iterator<Node<T>> iterator() {
        return new NodeIterator();
    }

    class NodeIterator implements Iterator<Node<T>> {
        private int index = 1;
        private Node<T> last = getFirst();

        public boolean hasNext() {
            return index <= size;
        }

        public Node<T> next() {
            Node<T> node = last;

            while(node.getIndex() != index) {
                if(node.getIndex() % 2 != 0) {
                    node = node.getMiddle();
                }
                else {
                    node = node.getMiddle().getNext();
                }
            }
            index++;
            return node;
        }

        public void remove() {
            throw new UnsupportedOperationException("not supported yet");
        }
    }
}
