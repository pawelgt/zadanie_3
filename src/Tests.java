import junit.framework.TestCase;
import java.util.Iterator;

/**
 * Created by Pawel on 2015-12-25.
 */
public class Tests extends TestCase {
    public void testEmptyListCreation() throws Exception{
        TripleList<Integer> list = new TripleList<>();

        assertEquals(0, list.getSize());
        assertEquals(null, list.getHead().getMiddle());
        assertEquals(null, list.getHead().getNext());
        assertEquals(null, list.getHead().getPrevious());
    }

    public void testAddingSingleElement() throws Exception {
        TripleList<Integer> list = new TripleList<>();
        Node<Integer> node = new Node<>(4);
        list.add(node);

        assertEquals(1, list.getSize());
        assertEquals(4, (int)list.getFirst().getValue());

        assertEquals(null, list.getFirst().getMiddle());
        assertEquals(null, list.getFirst().getNext());
        assertEquals(list.getHead(), list.getFirst().getPrevious());
    }

    public void testAddingTwoElements() throws Exception {
        TripleList<Integer> list = new TripleList<>();
        int value1 = 4;
        int value2 = -9;
        list.add(new Node<>(value1));
        list.add(new Node<>(value2));

        assertEquals(2, list.getSize());

        assertEquals(value1, (int) list.getFirst().getValue());
        assertEquals(value2, (int) list.getFirst().getMiddle().getValue());
        assertEquals(list.getFirst().getValue(), list.getFirst().getMiddle().getMiddle().getValue());

        assertNotNull(list.getFirst().getPrevious());
        assertNotNull(list.getFirst().getMiddle());
        assertNull(list.getFirst().getNext());

        assertNull(list.getFirst().getMiddle().getPrevious());
        assertNotNull(list.getFirst().getMiddle().getMiddle());
        assertNull(list.getFirst().getMiddle().getNext());
    }

    public void testAddingTreeElements() throws Exception {
        TripleList<Integer> list = new TripleList<>();
        int value1 = 4;
        int value2 = -9;
        int value3 = 47;
        list.add(new Node<>(value1));
        list.add(new Node<>(value2));
        list.add(new Node<>(value3));

        assertEquals(3, list.getSize());

        assertEquals(value1, (int) list.getFirst().getValue());
        assertEquals(value2, (int) list.getFirst().getMiddle().getValue());
        assertEquals(value3, (int) list.getFirst().getNext().getValue());

        assertNotNull(list.getFirst().getPrevious());
        assertNotNull(list.getFirst().getMiddle());
        assertNotNull(list.getFirst().getNext());

        assertNull(list.getFirst().getMiddle().getPrevious());
        assertNotNull(list.getFirst().getMiddle().getMiddle());
        assertNull(list.getFirst().getMiddle().getNext());

        assertNotNull(list.getFirst().getNext().getPrevious());
        assertNull(list.getFirst().getNext().getMiddle());
        assertNull(list.getFirst().getNext().getNext());
    }

    public void testAddingFiveElements() throws Exception {
        TripleList<Integer> list = new TripleList<>();
        int value1 = 1;
        int value2 = 2;
        int value3 = 3;
        int value4 = 4;
        int value5 = 5;
        list.add(new Node<>(value1));
        list.add(new Node<>(value2));
        list.add(new Node<>(value3));
        list.add(new Node<>(value4));
        list.add(new Node<>(value5));

        assertEquals(5, list.getSize());

        assertEquals(value1, (int) list.getFirst().getValue());
        assertEquals(value2, (int) list.getFirst().getMiddle().getValue());
        assertEquals(value3, (int) list.getFirst().getNext().getValue());
        assertEquals(value4, (int) list.getFirst().getNext().getMiddle().getValue());
        assertEquals(value5, (int) list.getFirst().getNext().getNext().getValue());
    }

    public void testListsEnumerator() throws Exception {
        double[] values = {1.1, 3.14, 6.13, 9.99999, 99.001};

        TripleList<Double> list = new TripleList<>();

        for (double value : values) {
            list.add(new Node<>(value));
        }

        int i = 0;

        for (Node<Double> node : list) {
            assertEquals(values[i++], node.getValue());
        }
    }

    public void testListsEnumerator2() throws Exception {
        double[] values = {1.1, 3.14, 6.13, 9.99999, 99.001};

        TripleList<Double> list = new TripleList<>();

        for (double value : values) {
            list.add(new Node<>(value));
        }

        int i = 0;

        Iterator<Node<Double>> it = list.iterator();
        while(it.hasNext()) {
            assertEquals(values[i++], it.next().getValue());
        }
    }

    public void testIfNoCycle() throws Exception {
        int size = 100;
        TripleList<Integer> listEverySingleNode = new TripleList<>();

        for (int i = 0; i < size; i++) {
            listEverySingleNode.add(new Node<>(i));
        }

        Node<Integer> single_node = listEverySingleNode.getFirst();
        Node<Integer> two_node = listEverySingleNode.getFirst().getNext();
        Iterator<Node<Integer>> it = listEverySingleNode.iterator();

        for (int i = 0; i < size * size; i++) {
            assertNotSame(single_node, two_node);
            single_node = it.next();

            if(two_node.getNext() == null) {
                break;
            }
            else {
                two_node = two_node.getNext();
            }
        }
    }

    public void testArrayInitializer() throws Exception {
        Integer[] values = {5, 10, 15};
        TripleList<Integer> list = new TripleList<>(values);

        Integer[] values2 = new Integer[5];
        values2[0] = 0;
        Iterator<Node<Integer>> it = list.iterator();
        for (int i = 0; i < list.getSize(); i++) {
            values2[i + 1] = it.next().getValue();
        }
        values2[4] = 20;
        TripleList<Integer> list2 = new TripleList<>(values2);

        assertEquals(3, list.getSize());
        assertEquals(5, list2.getSize());
        assertEquals(list.getFirst().getValue(), list2.getFirst().getMiddle().getValue());
    }
}